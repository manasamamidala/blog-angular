import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-propbind',
  template:
   '<input bind-disabled= "isDisabled" id="{{courseId}}" type= "text" value="angular10">',
  styles: ['']
})
export class PropbindComponent implements OnInit {
  public  courseId="123";
  public isDisabled =true;
  


  constructor() { }

  ngOnInit(): void {
  }

}
